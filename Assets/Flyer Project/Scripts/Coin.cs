﻿/*
 * Represents the coin.
 */

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Image))]
public class Coin : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameStateManager.GameState == GameState.Playing && 
            col.gameObject.layer == LayerMask.NameToLayer(Tags.Player))
        {
            GetComponent<AudioSource>().Play();
            GetComponent<Image>().enabled = false;
        }
    }

}
