﻿/*
 * Represents the flyer
 * In this class we will rotate the player, verify collision and manage the state of the game.
 * 
 * 
 * NOTE: For better organization we would prefer manage the screens (Intro, Play and Dead) in another script.
 *       But since it is a simple project I decided manage it here.
 */

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Rigidbody2D))]
public class Flyer : MonoBehaviour {

    enum TravelState { GoingUp, GoingDown }

    private TravelState travelState;
    private const int DIVERGENCE = 10;
    private const int AngleRotation = 20;

    public AudioClip flyClip;
    public AudioClip dieClip;
    
    public float RotateUpSpeed;
    public float RotateDownSpeed;

    public GameObject IntroGUI;
    public GameObject DeathGUI;

    public float VelocityPerJump;
    public float ForceJump;

    /// <summary>
    /// This will rotate when it is going up or going down, to simulate jump and fall 
    /// </summary>
    void FlyRotation()
    {
        if (GetComponent<Rigidbody2D>().velocity.y > 0)
            travelState = TravelState.GoingUp;
        else travelState = TravelState.GoingDown;

        float degreesToAdd = 0;
        switch (travelState)
        {
            case TravelState.GoingUp:
                degreesToAdd = 6 * RotateUpSpeed;
                if (transform.eulerAngles.z < AngleRotation || (transform.eulerAngles.z > 360 - AngleRotation - DIVERGENCE))
                    transform.Rotate(Vector3.forward, degreesToAdd);
                break;
            case TravelState.GoingDown:
                degreesToAdd = -3 * RotateDownSpeed;
                var minEulerAngle = 360 - AngleRotation;
                if (transform.eulerAngles.z > minEulerAngle || (transform.eulerAngles.z >= 0 && transform.eulerAngles.z < AngleRotation + DIVERGENCE))
                    transform.Rotate(Vector3.forward, degreesToAdd);
                break;
        }
    }

    void FlyImpulse()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, VelocityPerJump);
        GetComponent<AudioSource>().PlayOneShot(flyClip);
    }
    void JumpSamePlace()
    {
        var rigidBody = GetComponent<Rigidbody2D>();
        if (rigidBody.velocity.y < -1)
            rigidBody.AddForce(new Vector2(0, rigidBody.mass * ForceJump * Time.deltaTime));
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();


        switch (GameStateManager.GameState)
        {
            case GameState.Intro:

                if (WasTouchedOrClicked())
                {
                    GetComponent<Image>().enabled = true;
                    FlyImpulse();
                    GameStateManager.GameState = GameState.Playing;
                    IntroGUI.SetActive(false);
                }
                break;
            case GameState.Playing:
                if (WasTouchedOrClicked())
                    FlyImpulse();
                break;
            case GameState.Dead:
                if (GetComponent<Image>().enabled) AnimationDie();
                break;
        }
    }
    void FixedUpdate ()
    {
		if (GameStateManager.GameState == GameState.Intro)
        {
            JumpSamePlace();
        }
        else
        {
            FlyRotation();
        }
	}
    public void RestartLevel()
    {
        GameStateManager.GameState = GameState.Intro;
        ScoreManager.Score = 0;
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    //TODO improve the animation, that for now it is only hide the image
    void AnimationDie()
    {
        GetComponent<Image>().enabled = false;
    }

    bool WasTouchedOrClicked()
    {
        return (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0) ||
                (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended));
    }

    void Die()
    {
        GameStateManager.GameState = GameState.Dead;
        DeathGUI.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(dieClip);
    }




    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameStateManager.GameState != GameState.Playing) return;

        if (col.gameObject.layer == LayerMask.NameToLayer(Tags.Coin))
        {
            ScoreManager.Score++;
        }
        else if (col.gameObject.layer == LayerMask.NameToLayer(Tags.Obstacle))
        {
            Die();
        }

    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (GameStateManager.GameState == GameState.Playing && col.gameObject.layer == LayerMask.NameToLayer(Tags.Floor))
            Die();
    }
}
