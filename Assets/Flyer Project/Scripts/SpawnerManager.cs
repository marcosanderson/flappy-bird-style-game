﻿/*
 * This class will be responsible for manage the spawning of the new obstables 
 */

using UnityEngine;

[System.Serializable]
public struct Interval
{
    public float min;
    public float max;

    public float RandomInterval()
    {
        return Random.Range(min, max);
    }
}


public class SpawnerManager : MonoBehaviour {

    public GameObject SpawnObject;

    public Transform ParentForPipes;

    public Interval timeInterval;
    public Interval yAxisThreshold;


	void Start () {
        Spawn();
    }

    /// <summary>
    /// Verifies the game state is playing.
    /// Then instantiate the obstacle and set the position and scale
    /// </summary>
    void Spawn()
    {
        if (GameStateManager.GameState == GameState.Playing ||
            GameStateManager.GameState == GameState.Dead)
        {
            GameObject go = Instantiate(SpawnObject);
            go.transform.SetParent(transform.parent);
            go.transform.GetComponent<RectTransform>().anchoredPosition3D = transform.GetComponent<RectTransform>().anchoredPosition3D + new Vector3(0, yAxisThreshold.RandomInterval(), 0);
            go.transform.SetParent(ParentForPipes);
            go.transform.localScale = Vector3.one;
        }
        Invoke("Spawn", timeInterval.RandomInterval());
    }
}
