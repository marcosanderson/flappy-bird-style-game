﻿public static class Tags 
{
    public static readonly string Player = "Player";
    public static readonly string Obstacle = "Obstacle";
    public static readonly string Floor = "Floor";
    public static readonly string Coin = "Coin";
}
