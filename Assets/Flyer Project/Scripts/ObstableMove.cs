﻿/*
 * Responsible for move the obstables
 */
using UnityEngine;
public class ObstableMove : MonoBehaviour {

    public float speed;

	void Update () {
        transform.Translate(-Time.deltaTime * speed, 0, 0);
    }
}
