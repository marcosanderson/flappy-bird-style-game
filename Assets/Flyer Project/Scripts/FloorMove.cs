﻿/*
 * Class responsible for move the floor 
 */
using UnityEngine;

public class FloorMove : MonoBehaviour {

    public RectTransform[] floors = new RectTransform[3];
    public float speed;

    private Vector3 restartPosition;
    private RectTransform FloorTransform;

	void Start () {
        restartPosition = floors[2].anchoredPosition3D;
        FloorTransform = GetComponent<RectTransform>();
    }
	
	void Update ()
    {
        for (int i = 0; i < floors.Length; i++)
        {

            if (floors[i].anchoredPosition3D.x + floors[i].rect.width / 3 < FloorTransform.anchoredPosition3D.x)
            {
                floors[i].anchoredPosition3D = restartPosition;
            }

            floors[i].Translate(-Time.deltaTime * speed, 0, 0);
        }
    }
}
