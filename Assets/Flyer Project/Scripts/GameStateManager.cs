﻿/*
 * In this class is defined the state of the game.
 */

public enum GameState
{
    Intro,
    Playing,
    Dead
}

public class GameStateManager{

    public static GameState GameState { get; set; }
    static GameStateManager()
    {
        GameState = GameState.Intro;
    }
}
