﻿/*
 * This class is responsible for selecting randomically the background.
 */
using UnityEngine;
using UnityEngine.UI;

public class RandomBackgroundManager : MonoBehaviour {

    public Sprite[] Backgrounds;

	void Start () {

        if (Backgrounds != null)
            GetComponent<Image>().sprite  = Backgrounds[Random.Range(0,Backgrounds.Length)];
	}
}
