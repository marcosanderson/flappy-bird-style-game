﻿/*
 * This class is responsible for manage the score, showing it in Units, Tens and Hundreds.
 */
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Sprite[] NumberSprites = new Sprite[10];

    public Image Units, Tens, Hundreds;

    public ushort previousScore;
    public static ushort Score;

	void FixedUpdate ()
    {
		if(previousScore != Score)
        {
            if (Score < 10)
            {
                Units.sprite = NumberSprites[Score];
            }
            else if (Score < 100)
            {
                if (!Tens.IsActive())Tens.gameObject.SetActive(true);
                Tens.sprite = NumberSprites[Score / 10];
                Units.sprite = NumberSprites[Score % 10];
            }
            else if (Score < 1000)
            {
                if (!Hundreds.IsActive()) Hundreds.gameObject.SetActive(true);
                Hundreds.sprite = NumberSprites[Score/100];
                Tens.sprite = NumberSprites[(Score % 100) / 10];
                Units.sprite = NumberSprites[(Score % 100) % 10];
            }
        }
        previousScore = Score;
	}
}
