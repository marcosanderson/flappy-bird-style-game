﻿/*
 * Responsible for destroy the obstacles when it is not visible anymore. This will save memory.
 */
using UnityEngine;

public class ObstableDestroyer : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.layer == LayerMask.NameToLayer(Tags.Obstacle) ||
            collision.gameObject.layer == LayerMask.NameToLayer(Tags.Coin))
        {
            Destroy(collision.gameObject.transform.parent.gameObject);
        }
    }
}
